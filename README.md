##Test project

####Install:
`docker-compose up` will set up project for you

####Usage:
* navigate to http://localhost for demo api platform page
* api can be found on https://localhost:8443/ or http://localhost:8080/
* mail catcher can be found on http://localhost:1080/

####Note:
Api platform comes along with lots of built-in stuff which I left untouched. But I switched postgres to mysql because I do not know pgsql.
Also I left some migration garbage which is not used for now - DB creates from entity mappings 

####API queries:
API queries can be performed with api-platform built-in swagger's UI
Here is some curl requests if needed:

//GET posts
`curl -X GET "http://localhost:8080/posts" -H "accept: application/ld+json"`

//create user 
`curl -X POST "http://localhost:8080/users" -H "accept: application/ld+json" -H "Content-Type: application/ld+json" -d "{ \"name\": \"User 1\", \"email\": \"em@a.il\"}"`

//create post
`curl -X POST "http://localhost:8080/posts" -H "accept: application/ld+json" -H "Content-Type: application/ld+json" -d "{ \"name\": \"Post 1\", \"priceNet\": { \"amount\": 10, \"currency\": \"UAH\" }, \"url\": \"http://localhost\", \"status\": 0, \"createdBy\": \"/users/1\"}"`

//get users post
`curl -X GET "http://localhost:8080/users/1/posts" -H "accept: application/ld+json"`

####Console command:
Console command to set posts status:

```docker-compose exec php bin/console post:status:update 1```

 or 

```docker-compose exec php bin/console post:status:update 2```
