<?php

namespace App\Normalizer;

use App\Entity\MoneyEmbedded;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Class MoneyNormalizer.
 */
class EmbeddedMoneyNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = array()): array
    {
        /* @var MoneyEmbedded $object */

        return ['amount' => $object->getReadableAmount(), 'currency' => $object->getCurrency()];
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof MoneyEmbedded;
    }

    /**
     * {@inheritdoc}
     */
    public function denormalize($data, $class, $format = null, array $context = array()): MoneyEmbedded
    {
        if (is_numeric($data['amount'])) {
            $data['amount'] *= 100;
        }
        $moneyEmbedded = new MoneyEmbedded();
        $moneyEmbedded->setAmount($data['amount']);
        $moneyEmbedded->setCurrency($data['currency']);

        return $moneyEmbedded;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null): bool
    {
        return isset($data['amount'], $data['currency']) && $type === MoneyEmbedded::class;
    }
}
