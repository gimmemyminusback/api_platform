<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @ApiResource(
 *     normalizationContext={"groups"={"read_post"}},
 *     denormalizationContext={"groups"={"write_post"}},
 * )
 * @ORM\Entity(repositoryClass="App\Entity\Repository\PostRepository")
 */
class Post
{
    public const STATUS_AWAITING_MODERATION = 0;
    public const STATUS_ACTIVE = 1;

    use TimestampableEntity;

    /**
     * @var int The entity Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @Groups("read_post")
     */
    private $id;

    /**
     * @var string A nice person
     *
     * @ORM\Column
     *
     * @Assert\NotBlank
     *
     * @Groups({"read_post","write_post"})
     */
    public $name = '';

    /**
     * @var MoneyEmbedded money embeded
     *
     * @Assert\Valid()
     * @Assert\NotNull()
     *
     * @ORM\Embedded(class="App\Entity\MoneyEmbedded")
     *
     * @ApiProperty(attributes={"swagger_context": {"type": "object", "properties": {"amount": {"type": "integer"}, "currency":{"type": "string"} } }})
     *
     * @Groups({"read_post","write_post"})
     */
    private $priceNet;

    /**
     * @var string
     *
     * @Assert\Url()
     * @Assert\NotNull()
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @Groups({"read_post","write_post"})
     *
     * @ApiProperty(attributes={"swagger_context": {"type": "url", "example": "http://localhost" }})
     */
    private $url;

    /**
     * @var null|int
     *
     * @ApiProperty(attributes={"swagger_context": {"description": "STATUS_AWAITING_MODERATION = 0, STATUS_ACTIVE = 1"}})
     *
     * @ORM\Column(type="integer", nullable=false)
     *
     * @Assert\NotNull()
     * @Assert\Choice(callback="getAllStatuses")
     *
     * @Groups({"read_post","write_post"})
     */
    protected $status = self::STATUS_AWAITING_MODERATION;

    /**
     * @var User
     *
     * @Groups({"read_post","write_post"})
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="posts")
     *
     * @ApiProperty(attributes={"swagger_context": {"type": "string", "example": "/users/1" }})
     */
    private $createdBy;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public static function getAllStatuses(): array
    {
        return [self::STATUS_AWAITING_MODERATION, self::STATUS_ACTIVE];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return MoneyEmbedded
     */
    public function getPriceNet(): MoneyEmbedded
    {
        return $this->priceNet;
    }

    /**
     * @param MoneyEmbedded $priceNet
     */
    public function setPriceNet(MoneyEmbedded $priceNet): void
    {
        $this->priceNet = $priceNet;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int|null $status
     */
    public function setStatus(?int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return User
     */
    public function getCreatedBy(): User
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy(User $createdBy): void
    {
        $this->createdBy = $createdBy;
    }


}
