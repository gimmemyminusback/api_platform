<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"read_user"}},
 *     denormalizationContext={"groups"={"write_user"}},
 * )
 * @ORM\Entity
 */
class User
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int The entity Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @Groups("read_user")
     */
    private $id;

    /**
     * @var string A nice person
     *
     * @ORM\Column
     *
     * @Assert\NotBlank
     *
     * @Groups({"read_user","write_user"})
     */
    public $name;

    /**
     * @var string A nice person
     *
     * @ORM\Column
     *
     * @Assert\NotBlank
     * @Assert\Email()
     *
     * @Groups({"read_user","write_user"})
     */
    public $email;

    /**
     * @var []Post
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="createdBy", cascade={"persist"})
     *
     * @Groups({"read_user"})
     *
     * @ApiSubresource
     */
    private $posts;

    public function __construct()
    {
        $this->posts = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return Post[]|ArrayCollection Post
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    /**
     * @param array $posts
     */
    public function setPosts(?array $posts): void
    {
        $this->posts = $posts;
    }


}
