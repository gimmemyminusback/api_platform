<?php

namespace App\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Internal\Hydration\IterableResult;

/**
 * PostRepository.
 */
class PostRepository extends EntityRepository
{
    /**
     * @param int $status
     *
     * @return IterableResult
     */
    public function updatePostStatusIterate(int $status): IterableResult
    {
        return $this->createQueryBuilder('post')
            ->andWhere('post.status <> :status')
            ->setParameter('status', $status)
            ->getQuery()
            ->iterate();
    }
}
