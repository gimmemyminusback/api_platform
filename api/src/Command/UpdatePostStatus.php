<?php

namespace App\Command;

use App\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UpdatePostStatus.
 */
class UpdatePostStatus extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $doctrine;

    /**
     * UpdatePostStatus constructor.
     *
     * @param EntityManagerInterface $doctrine
     */
    public function __construct(EntityManagerInterface $doctrine)
    {
        parent::__construct();
        $this->doctrine = $doctrine;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('post:status:update')
            ->addArgument('status', InputArgument::REQUIRED, 'Status (int). 0 - awaiting moderation, 1 - active')
            ->setDescription('Updates posts status in bulk. Posts with same status wont be affected');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $status = $input->getArgument('status');
        if (!in_array($status, Post::getAllStatuses())) {
            $output->writeln('Status should be 0 or 1');
            return;
        }
        $status = (int)$status;
        $eventPointsIterator = $this->doctrine->getRepository(Post::class)
            ->updatePostStatusIterate($status);

        $i = 0;
        foreach ($eventPointsIterator as $item) {
            /** @var Post $post */
            $post = $item[0];

            $post->setStatus($status);
            ++$i;
            if (0 === ($i % 100)) {
                $this->doctrine->flush();
                $this->doctrine->clear();
            }
        }
        $this->doctrine->flush();
        $output->writeln('[SUCCESS] successfully updated ' . $i . ' posts');
    }
}
